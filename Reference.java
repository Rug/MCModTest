package rug.tutorial;

public class Reference {

	
	public static final String MOD_ID = "TutMod";
	public static final String NAME = "Rug's Tutorial Mod";
	public static final String VERSION = "1.0";
	public static final String ACCEPTED_VERSIONS = "[1.9,4]";
	
	public static final String CLIENT_PROXY_CLASS = "rug.tutorial.proxy.ClientProxy";
	public static final String SERVER_PROXY_CLASS = "rug.tutorial.proxy.ServerProxy";
}
